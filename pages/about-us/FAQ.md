---
title: F.A.Q.
description: Frequently asked questions about Veilid
weight: 6
layout: subpage
---

<ul>
<!--
  <li>
    <h3>Question</h3>
    <p>Answer</p>
  </li>
-->
  <li>
    <h3>Is Veilid looking for funding?</h3>
    <p>Veilid is not seeking venture capital or investment. We are accepting tax-deductible donations to our non-profit foundation, Veilid Foundation Inc.</p>
  </li>
  <li>
    <h3>Does Veilid have a cryptocurrency?</h3>
    Heck no. Veilid does not have a cryptocurrency. 
  </li>
  <li>
    <h3>Does Veilid use AI?</h3>
    Heck no. Veilid does not use AI. 
  </li>
  <li>
    <h3>Does Veilid use blockchain?</h3>
    Heck no. Veilid does not use blockchain. 
  </li>
</ul>

<div class="clearfix my-2">&nbsp;</div>


<div class="focus-text text-center">
  Have a question? Email <a href="mailto:press@veilid.org" class="text-white">press@veilid.org</a>
</div>

